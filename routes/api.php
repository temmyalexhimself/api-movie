<?php

use App\Http\Controllers\FoodController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\ReservedCategoryController;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'v1'], function(){
    Route::get('movies', [MovieController::class, 'index']);
    Route::get('movies/{id}', [MovieController::class, 'show']);

    Route::get('foods', [FoodController::class, 'index']);
    Route::get('food/{id}', [FoodController::class, 'show']);
    Route::get('food/show/{slug}', [FoodController::class, 'showBySlug']);

    Route::get('reserved-categories', [ReservedCategoryController::class, 'index']);
    Route::get('reserved-category/{id}', [ReservedCategoryController::class, 'show']);
    Route::get('reserved-category/show/{slug}', [ReservedCategoryController::class, 'showBySlug']);
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function(){
    Route::post('rating/store', [RatingController::class, 'store']);

    Route::patch('movies/update/{id}', [MovieController::class, 'update']);
});