<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Food;
use App\Models\Movie;
use App\Models\Rating;
use App\Models\ReservedCategory;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        // Category::factory(5)->create();
        // Movie::factory(1000)->create();
        // Rating::factory(10)->create();

        Food::factory(10)->create();
        ReservedCategory::factory(5)->create();

        // $this->call([
        //     UserSeeder::class
        // ]);
    }
}
