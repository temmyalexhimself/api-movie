<?php

namespace Database\Factories;

use App\Models\Food;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FoodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Food::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence();
        $slug = Str::slug($title, '-');
        
        return [
            'title' => $title,
            'description' => $this->faker->sentence(),
            'price' => $this->faker->randomDigit,
            'image_url' => 'https://source.unsplash.com/random',
            'status' => $this->faker->randomElement(['active' , 'inactive']),
            'slug' => $slug
        ];
    }
}
