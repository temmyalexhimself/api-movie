<?php

namespace Database\Factories;

use App\Models\ReservedCategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ReservedCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReservedCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence();
        $slug = Str::slug($title, '-');

        return [
            'name' => $this->faker->sentence(),
            'image_url' => 'https://source.unsplash.com/random',
            'slug' => $slug
        ];
    }
}
