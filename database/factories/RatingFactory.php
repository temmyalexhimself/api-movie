<?php

namespace Database\Factories;

use App\Models\Movie;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RatingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rating::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'rate' => rand(1, 5),
            'movie_id' => Movie::factory()->create()->id,
            'user_id' => User::factory()->create()->id,
            'status' => $this->faker->randomElement(['active', 'inactive'])
        ];
    }
}
