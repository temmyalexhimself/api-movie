<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rating extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRatingSummary()
    {
        return self::select(DB::raw('ROUND(AVG(rate), 1) AS rating'))
            ->where('movie_id', $this->movie_id)
            ->first();
    }
}
