<?php

namespace App\Http\Controllers;

use App\Http\Resources\RatingResource;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RatingController extends Controller
{
    public function store(Request $request)
    {
        $messages = [
            'rate.required' => 'Rating harus diisi',
            'movie_id.required' => 'Movie harus diisi'
        ];

        $rules = [
            'rate' => 'required|min:1',
            'movie_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 400);
        }

        $rating = Rating::create([
            'rate' => $request->rate,
            'movie_id' => $request->movie_id,
            'status' => 'active',
            'user_id' => $request->user()->id
        ]);

        return response(new RatingResource($rating), Response::HTTP_CREATED);
    }
}
