<?php

namespace App\Http\Controllers;

use App\Models\ReservedCategory;
use Illuminate\Http\Request;

class ReservedCategoryController extends Controller
{
    public function index()
    {
        $reserved_categories = ReservedCategory::orderBy('id', 'desc')->get();
        return response()->json($reserved_categories, 200);
    }

    public function showBySlug($slug)
    {
        $reserved_category = ReservedCategory::where('slug', $slug)->first();
        return response()->json($reserved_category, 200);
    }

    public function show($id)
    {
        $reserved_category = ReservedCategory::findOrFail($id);
        return response()->json($reserved_category, 200);
    }
}
