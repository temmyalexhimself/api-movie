<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function index()
    {
        $foods = Food::orderBy('id', 'desc')->get();
        return response()->json($foods, 200);
    }

    public function showBySlug($slug)
    {
        $food = Food::where('slug', $slug)->first();
        return response()->json($food, 200);
    }

    public function show($id)
    {
        $food = Food::findOrFail($id);
        return response()->json($food, 200);
    }
}
