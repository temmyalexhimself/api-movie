<?php

namespace App\Http\Controllers;

use App\Http\Resources\MovieResource;
use App\Models\Movie;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::with('category')->orderBy('created_at', 'desc')->take(100)->get();
        return response(MovieResource::collection($movies), Response::HTTP_OK);
    }

    public function show($id)
    {
        $movie = Movie::with('category')->findOrFail($id);
        return response(new MovieResource($movie), Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->update($request->only('title', 'description', 'category_id'));

        return response(new MovieResource($movie), Response::HTTP_ACCEPTED);
    }
}
